{
  "name": "Node.js",
  "description": "Build, test and analyse your JavaScript/TypeScript/[Node.js](https://nodejs.org/) projects",
  "template_path": "templates/gitlab-ci-node.yml",
  "kind": "build",
  "prefix": "node",
  "is_component": true,
  "variables": [
    {
      "name": "NODE_CONFIG_REGISTRY",
      "description": "npm [registry](https://docs.npmjs.com/cli/v8/using-npm/registry)    ",
      "type": "url",
      "advanced": true
    },
    {
      "name": "NODE_IMAGE",
      "description": "The Docker image used to run Node.js - **set the version required by your project**",
      "default": "registry.hub.docker.com/library/node:lts-alpine"
    },
    {
      "name": "NODE_MANAGER",
      "description": "The package manager used by your project (npm, yarn or pnpm) - **if undefined, automatic detection**",
      "default": "auto",
      "type": "enum",
      "values": ["auto", "npm", "yarn", "pnpm"],
      "advanced": true
    },
    {
      "name": "NODE_PROJECT_DIR",
      "description": "Node project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "NODE_SOURCE_DIR",
      "description": "Sources directory",
      "default": "src",
      "advanced": true
    },
    {
      "name": "NODE_CONFIG_SCOPED_REGISTRIES",
      "description": "Space separated list of NPM [scoped registries](https://docs.npmjs.com/cli/v8/using-npm/scope#associating-a-scope-with-a-registry) (formatted as `@somescope:https://some.npm.registry/some/repo @anotherscope:https://another.npm.registry/another/repo`)",
      "advanced": true
    },
    {
      "name": "NODE_BUILD_DISABLED",
      "description": "Set to true to disable build",
      "default": "false",
      "type": "boolean",
      "advanced": true
    },
    {
      "name": "NODE_BUILD_ARGS",
      "description": "npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments - yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments - pnpm [run script](https://pnpm.io/cli/run) arguments\n\n⚠ default value should be overridden for `pnpm` as `--prod` is not a valid option",
      "default": "run build --prod",
      "advanced": true
    },
    {
      "name": "NODE_BUILD_DIR",
      "description": "Variable to define build directory",
      "default": "dist",
      "advanced": true
    },
    {
      "name": "NODE_TEST_ARGS",
      "description": "npm [test](https://docs.npmjs.com/cli/v8/commands/npm-test) arguments - yarn [test](https://classic.yarnpkg.com/en/docs/cli/test) arguments - pnpm [test](https://pnpm.io/cli/test) arguments",
      "default": "test -- --coverage",
      "advanced": true
    },
    {
      "name": "NODE_INSTALL_EXTRA_OPTS",
      "description": "Extra options to install project dependencies (either [`npm ci`](https://docs.npmjs.com/cli/ci.html/), [`yarn install`](https://yarnpkg.com/cli/install) or [`pnpm install`](https://pnpm.io/cli/install))",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "node-lint",
      "name": "node lint",
      "description": "node lint analysis",
      "enable_with": "NODE_LINT_ENABLED",
      "variables": [
        {
          "name": "NODE_LINT_ARGS",
          "description": "npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments to execute the lint analysis - yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments to execute the lint analysis - pnpm [run script](https://pnpm.io/cli/run) arguments to execute the lint analysis",
          "default": "run lint",
          "advanced": true
        }
      ]
    },
    {
      "id": "node-audit",
      "name": "node audit",
      "description": "node audit analysis",
      "disable_with": "NODE_AUDIT_DISABLED",
      "variables": [
        {
          "name": "NODE_AUDIT_ARGS",
          "description": "npm [audit](https://docs.npmjs.com/cli/v8/commands/npm-audit) arguments - yarn [audit](https://classic.yarnpkg.com/en/docs/cli/audit) arguments - [pnpm audit](https://pnpm.io/cli/audit) arguments",
          "default": "--audit-level=low"
        }
      ]
    },
    {
      "id": "node-outdated",
      "name": "node outdated",
      "description": "node outdated analysis",
      "disable_with": "NODE_OUTDATED_DISABLED",
      "variables": [
        {
          "name": "NODE_OUTDATED_ARGS",
          "description": "npm [outdated](https://docs.npmjs.com/cli/v8/commands/npm-outdated) arguments - yarn [outdated](https://classic.yarnpkg.com/lang/en/docs/cli/outdated/) arguments - pnpm [outdated](https://pnpm.io/cli/outdated) arguments",
          "default": "--long"
        }
      ]
    },
    {
      "id": "node-semgrep",
      "name": "Semgrep",
      "description": "[Semgrep](https://semgrep.dev/docs/) analysis",
      "disable_with": "NODE_SEMGREP_DISABLED",
      "variables": [
        {
          "name": "NODE_SEMGREP_IMAGE",
          "description": "The Docker image used to run [Semgrep](https://semgrep.dev/docs/)",
          "default": "registry.hub.docker.com/semgrep/semgrep:latest"
        },
        {
          "name": "NODE_SEMGREP_ARGS",
          "description": "Semgrep [scan options](https://semgrep.dev/docs/cli-reference#semgrep-scan-command-options)",
          "default": "--metrics off --disable-version-check --no-suppress-errors"
        },
        {
          "name": "NODE_SEMGREP_RULES",
          "description": "Space-separated list of [Semgrep rules](https://semgrep.dev/docs/running-rules).\n\nCan be both local YAML files or remote rules from the [Semgrep Registry](https://semgrep.dev/explore) (denoted by the `p/` prefix).",
          "default": "p/javascript p/eslint p/gitlab-eslint"
        },
        {
          "name": "NODE_SEMGREP_REGISTRY_BASE_URL",
          "description": "The Semgrep Registry base URL that is used to download the rules. No trailing slash.",
          "default": "https://semgrep.dev/c"
        },
        {
          "name": "NODE_SEMGREP_DOWNLOAD_RULES_ENABLED",
          "description": "Download Semgrep remote rules",
          "type": "boolean",
          "default": "true"
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [@cyclonedx/cyclonedx-npm](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm)",
      "disable_with": "NODE_SBOM_DISABLED",
      "variables": [
        {
          "name": "NODE_SBOM_VERSION",
          "description": "Version of the @cyclonedx/cyclonedx-npm used for SBOM analysis",
	  "advanced": true
        },
        {
          "name": "NODE_SBOM_OPTS",
          "description": "Options for @cyclonedx/cyclonedx-npm used for SBOM analysis",
          "default": "--omit dev",
          "advanced": true
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish",
      "description": "[publishes](https://docs.npmjs.com/cli/v8/commands/npm-publish) the project package to a npm registry",
      "enable_with": "NODE_PUBLISH_ENABLED",
      "variables": [
        {
          "name": "NODE_PUBLISH_ARGS",
          "description": "npm [publish](https://docs.npmjs.com/cli/v8/commands/npm-publish) extra arguments - yarn [publish](https://classic.yarnpkg.com/lang/en/docs/cli/publish/) extra arguments - pnpm [publish](https://pnpm.io/cli/publish) extra arguments",
          "advanced": true
        },
        {
          "name": "NODE_PUBLISH_TOKEN",
          "description": "npm publication registry authentication token",
          "secret": true
        }
      ]
    }
  ],
  "variants": [  
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-node-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
         "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
       }
      ]
    }
  ]
}
